# Specify the provider and access details
provider "aws" {
  region     = "us-east-1"
  access_key = "${var.aws-access-key}"
  secret_key = "${var.aws-secret-key}"
}


# Default security group to access
# the instances over SSH and HTTP
resource "aws_security_group" "web" {
#  name        = "springcoredemo-security-group-from-terrorform" #optional, when omitted, terraform creates a random name
  description = "security group for springcoredemo web created from terraform"
  vpc_id      = "vpc-c422e2a0"

  # SSH access from anywhere
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # HTTP access from the VPC
  ingress {
    from_port   = 8000
    to_port     = 8000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # HTTP access from the VPC
  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Our default security group to for the database
resource "aws_security_group" "rds" {
  description = "security group for springcoredemo RDS created from terraform"
  vpc_id      = "vpc-c422e2a0"

  # mysql access from anywhere
  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_db_instance" "default" {
  depends_on             = ["aws_security_group.rds"]
#  identifier             = "springcoredemo-rds" # Terraform will create a unique id if not assigned
  allocated_storage      = "10"
  engine                 = "mysql"
  engine_version         = "8.0.15"
  instance_class         = "db.t2.micro"
  name                   = "springcoredemo"
  username               = "root"
  password               = "6969Cutlass!!"
  vpc_security_group_ids = ["${aws_security_group.rds.id}"]
}

resource "aws_instance" "web" {
  # The connection block tells our provisioner how to
  # communicate with the resource (instance)
  connection {
    # The default username for our AMI
    user = "ubuntu"
 }

  instance_type = "t2.micro"
  
  tags { Name = "springcoredemo instance" } 

  # standard realmethods community AMI with docker pre-installed
  ami = "ami-05033408e5e831fb0"

  # The name of the  SSH keypair you've created and downloaded
  # from the AWS console.
  #
  # https://console.aws.amazon.com/ec2/v2/home?region=us-east-1#KeyPairs:
  #
  key_name = "my-public-key"
  
  # Our Security group to allow HTTP and SSH access
  vpc_security_group_ids = ["${aws_security_group.web.id}"]

  provisioner "remote-exec" {
    inline = [
      "sudo apt-get -y update",
      "sudo docker login --username tylertravismya --password 69cutlass",
      "sudo docker pull realmethods/springcoredemo:latest",
      "sudo docker run -it -p 8000:8000 -p 8080:8080 -e DATABASE_URL=jdbc:mysql://${aws_db_instance.default.endpoint}/springcoredemo realmethods/springcoredemo:latest"
    ]
  }
}
